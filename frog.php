<?php
class Frog extends Animal
{
  public $legs = 2;
  public $cold_blooded = true;
  public function jump()
  {
    echo "hop hop";
  }
}

?>
