<?php
include('animal.php');
include('frog.php');
include('ape.php');

$sheep = new Animal("shaun");

echo $sheep->name . "<br>";
echo $sheep->legs . "<br>";
echo $sheep->cold_blooded;

echo "<br>";

$sungokong = new Ape("kera sakti");
$sungokong->yell();

echo "<br> <br>";

$kodok = new Frog("buduk");
$kodok->jump();
?>
